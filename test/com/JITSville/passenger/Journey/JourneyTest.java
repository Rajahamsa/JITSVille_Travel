package com.JITSville.passenger.Journey;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.JITSville.passenger.bean.PassengerBean;

public class JourneyTest {
	List<PassengerBean> list;
	private PassengerBean pb;
	private Journey j;

	@Before
	public void setUp() {
		j=new Journey();
		list=new ArrayList<PassengerBean>();
		pb=new PassengerBean("Raji", 3, 600, true, false, true);
		list.add(pb);
		pb=new PassengerBean("Raji", 5, 600, true, false, true);
		list.add(pb);
		pb=new PassengerBean("Raji", 4, 600, false, false, true);
		list.add(pb);
		pb=new PassengerBean("Raji", 5, 90, false, true, true);
		list.add(pb);
		pb=new PassengerBean("Raji", 5, 199, false, true, true);
		list.add(pb);
	}

	@Test
	public void testTotalNoOfPaper() {
		int expected=5;
		int actual=j.totalNoOfPaper(list);
		assertEquals(expected,actual);
		
	}

	@Test
	public void testTotalNoOfMeals() {
		int expected=2;
		int actual=j.totalNoOfMeals(list);
		assertEquals(expected,actual);
	}

	@Test
	public void testTotalCostforallPassengers() {
		double expected=150.1;
		double actual=j.totalCostforallPassengers(list);
		assertEquals(expected,actual,001);
	}

}

package com.JITSville.passenger;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.JITSville.passenger.bean.PassengerBean;
import com.JITSville.passenger.factory.PassengerFactory;

public class CommuterPassengerTest {
	
	private Passenger p;
	private PassengerBean pb;
	private PassengerFactory pf;

	@Before
	public void setUp() {
		pf=new PassengerFactory();
		p=pf.getPassenger("COMMUTERPASSENGER");
	}

	//frequentRider
	@Test
	public void testCostfrequentRider() {
		pb=new PassengerBean("Raji", 5,200 ,true, false, true);
		double expected=2.25;
		double actual=p.cost(pb);
		assertEquals(expected,actual,001);
		
	}
	
	@Test
	public void testCostnotfrequentRider() {
		pb=new PassengerBean("Raji", 5,200 ,false, false, true);
		double expected=2.50;
		double actual=p.cost(pb);
		assertEquals(expected,actual,001);
		
	}

}

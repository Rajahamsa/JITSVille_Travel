package com.JITSville.passenger;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.JITSville.passenger.bean.PassengerBean;
import com.JITSville.passenger.factory.PassengerFactory;
import com.JITSville.passenger.meal.Meal;

public class VacationPassengerTest {
	
	private Passenger p;
	private PassengerBean pb;
	private Meal m;
	private PassengerFactory pf;
	
	@Rule
	public ExpectedException ie=ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		pf=new PassengerFactory();
		p=pf.getPassenger("VACATIONPASSENGER");
		m=(Meal) pf.getPassenger("VACATIONPASSENGER");
	}

	@Test
	public void testCost() {
		pb=new PassengerBean("Raji", 5, 300,false, true, true);
		double expected=150;
		double actual=p.cost(pb);
		assertEquals(expected,actual,001);
		
	}
	
	@Test
	public void testCostExceptionLess() {
		pb=new PassengerBean("Raji", 5,3,false, true, true);
		ie.expect(IllegalArgumentException.class);
		ie.expectMessage("Vacationers cannot travel less than five miles or more than 4000 miles.");
		double actual=p.cost(pb);
		
	}
	
	@Test
	public void testCostExceptionGreater() {
		pb=new PassengerBean("Raji", 5,40000,false, true, true);
		ie.expect(IllegalArgumentException.class);
		ie.expectMessage("Vacationers cannot travel less than five miles or more than 4000 miles.");
		double actual=p.cost(pb);
		
	}
	
	@Test
	public void testMealPerPassenger() {
		pb=new PassengerBean("Raji", 5,400,false, true, true);
		int expected=4;
		int actual=m.mealPerPassenger(pb);
		assertEquals(expected,actual);
	}


}

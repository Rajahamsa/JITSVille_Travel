package com.JITSville.passenger;

import static org.junit.Assert.*;


import org.junit.Test;

/**
 * @author rjanagam
 *
 */
public class PassengerTest {
	
	// Test Ratefactor
	@Test
	public void test() {
		double expectedRatefactor=0.5;
		double actualRatefactor=Passenger.RATE_FACTOR;
		assertEquals(expectedRatefactor,actualRatefactor,001);
	}

}

package com.JITSville.passenger.meal;

import com.JITSville.passenger.bean.PassengerBean;

public interface Meal {
	
	int mealPerPassenger(PassengerBean pb);
	
}

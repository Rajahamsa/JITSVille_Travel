package com.JITSville.passenger;

import com.JITSville.passenger.bean.PassengerBean;
import com.JITSville.passenger.meal.Meal;

public class VacationPassenger implements Passenger,Meal{

	@Override
	public double cost(PassengerBean p) {
		double charge;
		if(p.getNoOfMiles()<5||p.getNoOfMiles()>4000) {
			throw new IllegalArgumentException("Vacationers cannot travel less than five miles or more than 4000 miles.");
		}else {
		charge=RATE_FACTOR*p.getNoOfMiles();
		}
		return charge;
	}

	@Override
	public int mealPerPassenger(PassengerBean pb) {
		int noOfMeals=0;
		if(pb.isMeal()) {
			 noOfMeals=pb.getNoOfMiles()/100;
			 if((pb.getNoOfMiles()%100<100) && (pb.getNoOfMiles()%100>0) ) {
				 noOfMeals++;
			 }
		}
		return noOfMeals;
	}

}

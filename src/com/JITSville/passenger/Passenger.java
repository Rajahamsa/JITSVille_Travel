package com.JITSville.passenger;

import com.JITSville.passenger.bean.PassengerBean;

/**
 * @author rjanagam
 * 
 * Interface that has common properties for passengers
 *
 */
public interface Passenger {

	double RATE_FACTOR=0.5;
	
	double cost(PassengerBean p);
}

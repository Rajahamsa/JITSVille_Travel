package com.JITSville.passenger;

import com.JITSville.passenger.bean.PassengerBean;

public class CommuterPassenger implements Passenger {

	@Override
	public double cost(PassengerBean p) {
		double charge;
		charge = RATE_FACTOR * p.getNoOfStops();
		if (p.isFrequentRider()) {
			charge = charge - RATE_FACTOR * 0.1 * p.getNoOfStops();
		} else {
			return charge;
		}
		return charge;
	}

}

package com.JITSville.passenger.factory;

import com.JITSville.passenger.CommuterPassenger;
import com.JITSville.passenger.Passenger;
import com.JITSville.passenger.VacationPassenger;

public class PassengerFactory {

	// use getPassenger method to get object of type Passenger
	public Passenger getPassenger(String passengerType) {

		if ("COMMUTERPASSENGER".equalsIgnoreCase(passengerType)) {
			return new CommuterPassenger();

		} else if ("VACATIONPASSENGER".equalsIgnoreCase(passengerType)) {
			return new VacationPassenger();

		}

		return null;
	}
}

package com.JITSville.passenger.Journey;

import java.util.List;

import com.JITSville.passenger.Passenger;
import com.JITSville.passenger.bean.PassengerBean;
import com.JITSville.passenger.factory.PassengerFactory;

public class Journey {

	int totalNoOfPaper(List<PassengerBean> list) {
		int totalNewsPaper = 0;
		for (PassengerBean pb : list) {
			if (pb.isNewspaper()) {
				totalNewsPaper++;
			}
		}

		return totalNewsPaper;

	}

	int totalNoOfMeals(List<PassengerBean> list) {
		int totalMeals = 0;
		for (PassengerBean pb : list) {
			if (pb.isMeal()) {
				totalMeals++;
			}
		}

		return totalMeals;

	}

	double totalCostforallPassengers(List<PassengerBean> list) {
		double totalCost = 0;
		PassengerFactory pf = new PassengerFactory();
		Passenger p;
		for (PassengerBean pb : list) {
			if (!(pb.isFrequentRider()) && (pb.isMeal())) {
				p = pf.getPassenger("VACATIONPASSENGER");
				totalCost = totalCost + p.cost(pb);

			} else {
				p = pf.getPassenger("COMMUTERPASSENGER");
				totalCost = totalCost + p.cost(pb);

			}
		}

		return totalCost;

	}
}

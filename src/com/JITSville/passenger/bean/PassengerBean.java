package com.JITSville.passenger.bean;

/**
 * @author rjanagam
 * 
 *         PassengerBean class is a pojo that has information related to
 *         passenger
 *
 */
public class PassengerBean {
	private String name;
	private int noOfStops;
	private int noOfMiles;
	private boolean frequentRider;
	private boolean meal;
	private boolean newspaper;

	public PassengerBean(String name, int noOfStops, int noOfMiles, boolean frequentRider, boolean meal,
			boolean newspaper) {
		this.name = name;
		this.noOfStops = noOfStops;
		this.noOfMiles = noOfMiles;
		this.frequentRider = frequentRider;
		this.meal = meal;
		this.newspaper = newspaper;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNoOfStops() {
		return noOfStops;
	}

	public void setNoOfStops(int noOfStops) {
		this.noOfStops = noOfStops;
	}

	public int getNoOfMiles() {
		return noOfMiles;
	}

	public void setNoOfMiles(int noOfMiles) {
		this.noOfMiles = noOfMiles;
	}

	public boolean isFrequentRider() {
		return frequentRider;
	}

	public void setFrequentRider(boolean frequentRider) {
		this.frequentRider = frequentRider;
	}

	public boolean isMeal() {
		return meal;
	}

	public void setMeal(boolean meal) {
		this.meal = meal;
	}

	public boolean isNewspaper() {
		return newspaper;
	}

	public void setNewspaper(boolean newspaper) {
		this.newspaper = newspaper;
	}

	@Override
	public String toString() {
		return "PassengerBean [name=" + name + ", noOfStops=" + noOfStops + ", noOfMiles=" + noOfMiles
				+ ", frequentRider=" + frequentRider + ", meal=" + meal + ", newspaper=" + newspaper + "]";
	}

}
